#ifndef CHANNELSUBSCRIPTIONS_H
#define CHANNELSUBSCRIPTIONS_H

#include <QSet>
#include "ChannelType.h"

typedef QSet<ChannelType> ChannelSubscriptions;

#endif // CHANNELSUBSCRIPTIONS_H
