#ifndef REWARDSSERVICE_H
#define REWARDSSERVICE_H

#include <memory>
#include <QList>
#include <QSet>
#include "AccountNumber.h"
#include "ChannelSubscriptions.h"

class EligibilityService;

class RewardsService
{
public:
    enum class RewardType
    {
        NONE,
        CHAMPIONS_LEAGUE_FINAL_TICKET,
        KARAOKE_PRO_MICROPHONE,
        PIRATES_OF_THE_CARIBBEAN_COLLECTION,
    };

    RewardsService(std::unique_ptr<EligibilityService> eligibilityService);


    QList<RewardType> getAvailableRewards( const AccountNumber& accNumber, const ChannelSubscriptions& subs );

private:
    RewardType getRewardForChannel( ChannelType channel);
    QList<RewardType> getAllRewards(const ChannelSubscriptions& channelSubs);

private:
    std::unique_ptr<EligibilityService> m_eligibilityService;
};

#endif // REWARDSSERVICE_H
