#ifndef MOCKELIGIBILITYSERVICE_H
#define MOCKELIGIBILITYSERVICE_H

#include "EligibilityService.h"

// This could be done with GMOCK
class MockEligibilityService: public EligibilityService
{
public:
    ErrorCode getEligibility(const AccountNumber &, EligibilityService::CustomerEligibleType &) override;

    void setMockResult(ErrorCode errorCode, CustomerEligibleType eligibleType);

private:
    ErrorCode m_mockErrorCode;
    CustomerEligibleType m_mockEligibleType;

};

#endif // MOCKELIGIBILITYSERVICE_H
