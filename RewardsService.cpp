#include "RewardsService.h"
#include "EligibilityService.h"
#include <QMap>

RewardsService::RewardsService(std::unique_ptr<EligibilityService> eligibilityService)
    :m_eligibilityService(std::move(eligibilityService))
{
}

QList<RewardsService::RewardType> RewardsService::getAvailableRewards(const AccountNumber &accNumber, const ChannelSubscriptions &channelSubs)
{
    EligibilityService::CustomerEligibleType eligibleStatus;
    EligibilityService::ErrorCode errCode = m_eligibilityService->getEligibility(accNumber, eligibleStatus);
    if (errCode != EligibilityService::ErrorCode::NONE) {
        // handle the error
        return QList<RewardsService::RewardType>();
    }

    if (eligibleStatus != EligibilityService::CustomerEligibleType::CUSTOMER_ELIGIBLE) {
        // handle the error
        return QList<RewardsService::RewardType>();
    }

    return getAllRewards(channelSubs);
}

RewardsService::RewardType RewardsService::getRewardForChannel(ChannelType channel)
{
    static const QMap<ChannelType, RewardsService::RewardType> channelRewardMap{
        {ChannelType::SPORTS, RewardType::CHAMPIONS_LEAGUE_FINAL_TICKET},
        {ChannelType::MUSIC, RewardType::KARAOKE_PRO_MICROPHONE},
        {ChannelType::MOVIES, RewardType::PIRATES_OF_THE_CARIBBEAN_COLLECTION}
    };

    return channelRewardMap.value(channel, RewardType::NONE);
}

QList<RewardsService::RewardType> RewardsService::getAllRewards(const ChannelSubscriptions &channelSubs)
{
    QList<RewardsService::RewardType> result;

    for ( ChannelType channel: channelSubs) {
        auto reward = getRewardForChannel(channel);
        if (reward != RewardsService::RewardType::NONE) {
            result << reward;
        }
    }

    return result;
}
