#include "TestRewardsService.h"

TestRewardsService::TestRewardsService(QObject *parent)
    :QObject(parent)
{
}

void TestRewardsService::initTestCase()
{
    //keep a ptr for mocking the result
    mockEligibilityService = new MockEligibilityService();

    auto ptr = std::unique_ptr<EligibilityService>(mockEligibilityService);
    rewardsService.reset( new RewardsService(std::move(ptr)));

    allChannels << ChannelType::SPORTS
                << ChannelType::KIDS
                << ChannelType::MUSIC
                << ChannelType::NEWS
                << ChannelType::MOVIES
                << ChannelType::NONE;
}

void TestRewardsService::test_InvalidCustomerNumberReturnsEmptyResult()
{
    mockEligibilityService->setMockResult(
                            EligibilityService::ErrorCode::INVALID_ACCOUNT_NUMBER,
                            EligibilityService::CustomerEligibleType::CUSTOMER_INELIGIBLE
                );

    auto result = rewardsService->getAvailableRewards( AccountNumber("dummy-account"), allChannels );
    QVERIFY( result.empty());
}

void TestRewardsService::test_IneligibleCustomerReturnsEmptyResult()
{
    mockEligibilityService->setMockResult(
                            EligibilityService::ErrorCode::NONE,
                            EligibilityService::CustomerEligibleType::CUSTOMER_INELIGIBLE
                );

    auto result = rewardsService->getAvailableRewards( AccountNumber("dummy-account"), allChannels );
    QVERIFY( result.empty());
}

void TestRewardsService::test_EligibleCustomerReturnsNonEmptyResult()
{
    mockEligibilityService->setMockResult(
                            EligibilityService::ErrorCode::NONE,
                            EligibilityService::CustomerEligibleType::CUSTOMER_ELIGIBLE
                );

    auto result = rewardsService->getAvailableRewards( AccountNumber("dummy-account"), allChannels );
    QVERIFY( !result.empty());
}

/// TODO
/// More tests can be added for cases such as:
/// Eligible customer, empty channels -> empty result
/// Eligible customer, spefic channel -> specific reward
/// etc
