#ifndef ELIGIBILITYSERVICE_H
#define ELIGIBILITYSERVICE_H

#include "AccountNumber.h"

class EligibilityService
{
public:
    virtual ~EligibilityService() = default;

    /// Enum with the eligiblity response
    enum class CustomerEligibleType
    {
        CUSTOMER_ELIGIBLE,
        CUSTOMER_INELIGIBLE
    };

    /// Enum containing all possible errors that might occur while checking for eligibility
    enum class ErrorCode
    {
        NONE,
        TECHNICAL_FAILURE,
        INVALID_ACCOUNT_NUMBER
    };

    virtual ErrorCode getEligibility(const AccountNumber&, CustomerEligibleType& ) = 0;
};

#endif // ELIGIBILITYSERVICE_H
