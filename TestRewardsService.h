#ifndef TESTREWARDSSERVICE_H
#define TESTREWARDSSERVICE_H

#include <QObject>
#include <QtTest/QtTest>
#include <memory>
#include "MockEligibilityService.h"
#include "RewardsService.h"

class TestRewardsService : public QObject
{
    Q_OBJECT
public:
    explicit TestRewardsService(QObject *parent = nullptr);

signals:

public slots:


private slots:
    void initTestCase();

    void test_InvalidCustomerNumberReturnsEmptyResult();
    void test_IneligibleCustomerReturnsEmptyResult();
    void test_EligibleCustomerReturnsNonEmptyResult();

private:
    QScopedPointer<RewardsService> rewardsService;
    MockEligibilityService* mockEligibilityService;
    QSet<ChannelType> allChannels;

};

#endif // TESTREWARDSSERVICE_H
