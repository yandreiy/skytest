#include "MockEligibilityService.h"

EligibilityService::ErrorCode MockEligibilityService::getEligibility(const AccountNumber &, EligibilityService::CustomerEligibleType & eligibleType)
{
    eligibleType = m_mockEligibleType;
    return m_mockErrorCode;
}

void MockEligibilityService::setMockResult(EligibilityService::ErrorCode errorCode, CustomerEligibleType eligibleType)
{
    m_mockErrorCode = errorCode;
    m_mockEligibleType = eligibleType;
}
