#ifndef CHANNELTYPE_H
#define CHANNELTYPE_H

#include <inttypes.h>
enum class ChannelType: uint8_t
{
    NONE,
    SPORTS,
    KIDS,
    MUSIC,
    NEWS,
    MOVIES,
};
inline uint qHash( ChannelType type, uint seed)
{
    return ::qHash( static_cast<uint>(type), seed);
}

#endif // CHANNELTYPE_H
